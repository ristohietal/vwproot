extern crate chrono;
extern crate image;
extern crate rand;
extern crate regex;
extern crate reqwest;
extern crate scraper;
extern crate tweetust;

use chrono::prelude::*;
use image::{GenericImageView, Pixel, GenericImage, ImageBuffer, Rgba, DynamicImage, FilterType};
use scraper::{Html, Selector};
use rand::thread_rng;
use rand::prelude::SliceRandom;
use regex::Regex;
use reqwest::Client;

use std::env;
use std::fs;
use std::io;
use std::io::prelude::*;

fn main() {
    let twitter_secrets: (String, String, String, String) =
        (env::var("CONSUMER_KEY").unwrap(),
         env::var("CONSUMER_SECRET").unwrap(),
         env::var("TOKEN").unwrap(),
         env::var("TOKEN_SECRET").unwrap());
    let output_dir: String = env::var("OUTDIR").unwrap();
    let asset_dir: String = env::var("ASSETDIR").unwrap();

    let date = Utc::now().format("%d.%m.%Y").to_string();
    match get_strip(asset_dir, output_dir) {
        Some((url, strip_filename)) => tweet(date, url, strip_filename, twitter_secrets),
        None => {}
    }
}

fn tweet(date: String, url: String, strip_filename: String, secrets: (String, String, String, String)) {
    use tweetust::*;

    let client = TwitterClient::new(
        OAuthAuthenticator::new(secrets.0, secrets.1, secrets.2, secrets.3),
        DefaultHttpHandler::with_https_connector().unwrap());
    let mut f = fs::File::open(strip_filename).unwrap();
    let mut buf = Vec::new();
    f.read_to_end(&mut buf).unwrap();
    let mut buf_reader = io::Cursor::new(&buf);

    println!("Tweeting {}", url);

    let media_res = client.media()
        .upload()
        .media(&mut buf_reader)
        .execute()
        .unwrap();
    let _status_res = client.statuses()
        .update(format!("Original © Juba Tuomola / HS {}\n{}", date, url))
        .media_ids(Some(media_res.object.media_id))
        .execute()
        .unwrap();
}

fn get_strip(asset_dir: String, output_dir: String) -> Option<(String, String)> {
    let mut rng = thread_rng();
    let proots = vec![
        image::open(asset_dir.clone() + "proot1.png").unwrap(),
        image::open(asset_dir.clone() + "proot2.png").unwrap(),
        image::open(asset_dir.clone() + "proot3.png").unwrap(),
        // image::open(asset_dir.clone() + "proot4.png").unwrap(), // röyhtäisy
    ];

    println!("Scraping https://www.hs.fi/viivijawagner/");

    let client = Client::new();
    let resp = client.get("https://www.hs.fi/viivijawagner/").send().unwrap().text().unwrap();
    let document = Html::parse_document(&resp);

    /* Single dom element with wanted information

    <figure class="cartoon image scroller"
            itemscope
            itemtype="http://schema.org/ImageObject"
            id="cartoon-2000005968343">
      <img id="image-4900207"
           class="lazyload lazyloadable-image "
           data-alt=""
           data-simple-src="//hs.mediadelivery.fi/img/320/c2095d32178e415f80e6cf1fdc7daf2d.jpg"
           data-srcset="//hs.mediadelivery.fi/img/1920/c2095d32178e415f80e6cf1fdc7daf2d.jpg 1920w"
      />
      <meta itemprop="contentUrl" content="/viivijawagner/car-2000005968343.html">
      <meta itemprop="datePublished" content="2019-01-19">
    </figure>
    */

    let figure_selector = Selector::parse("figure.cartoon").unwrap();
    let img_selector = Selector::parse("img").unwrap();
    let url_selector = Selector::parse(r#"meta[itemprop="contentUrl"]"#).unwrap();
    let date_selector = Selector::parse(r#"meta[itemprop="datePublished"]"#).unwrap();

    for figure_elem in document.select(&figure_selector) {
        let img_elem = figure_elem.select(&img_selector).next().unwrap();
        let url_elem = figure_elem.select(&url_selector).next().unwrap();
        let date_elem = figure_elem.select(&date_selector).next().unwrap();

        let img_path = img_elem.value().attr("data-simple-src").unwrap();
        let re = Regex::new(r"hs.mediadelivery.fi/img/\d+/(.+jpg)").unwrap();
        let strip_filename = re.captures(img_path).unwrap().get(1).unwrap().as_str();

        let strip_url = url_elem.value().attr("content").unwrap();
        let strip_url = "https://www.hs.fi".to_owned() + strip_url;
        let strip_date = date_elem.value().attr("content").unwrap();
        let date = Utc::now().format("%Y-%m-%d").to_string();

        if strip_date == date {
            let strip = get_image(strip_filename, &client);
            let strip = get_cropped(strip);
            let strip = overlay(strip, proots.choose(&mut rng).unwrap());

            let out_filename: String = output_dir + strip_filename;
            strip.save(out_filename.as_str()).unwrap();

            return Some((strip_url, out_filename));
        }
    }
    None
}

fn get_image(strip_filename: &str, client: &Client) -> DynamicImage {
    println!("Loading image {}", strip_filename);
    let mut buffer: Vec<u8> = vec![];
    let url: String = "https://hs.mediadelivery.fi/img/1920/".to_owned() + strip_filename;
    let mut resp = client.get(url.as_str()).send().unwrap();
    resp.copy_to(&mut buffer).unwrap();
    image::load_from_memory_with_format(&buffer, image::ImageFormat::JPEG).unwrap()
}

fn overlay(mut image: ImageBuffer<Rgba<u8>, Vec<u8>>, overlay_image: &DynamicImage) -> ImageBuffer<Rgba<u8>, Vec<u8>> {
    println!("Overlaying image");
    let overlay_pos_x = image.width() / 3 + 5;
    let overlay_image_resized =
        overlay_image.resize(image.width(), image.height(), FilterType::Lanczos3);
    image::imageops::overlay(&mut image, &overlay_image_resized.to_rgba(), overlay_pos_x, 0);
    image
}

fn get_cropped(mut image: DynamicImage) -> ImageBuffer<Rgba<u8>, std::vec::Vec<u8>> {
    println!("Cropping image");
    let max_x: u32 = image.width();
    let max_y: u32 = image.height();
    let center_x: u32 = max_x / 2;
    let center_y: u32 = max_y / 2;
    let mut padding_left = 0;
    let mut padding_top = 0;
    let mut padding_right = 0;
    let mut padding_bottom = 0;

    for x in 0..center_x {
        if image.get_pixel(x, center_y).to_rgb().data != [255, 255, 255] {
            padding_left =
                if x < 10 { 0 } else { x - 10 };
            break;
        }
    }

    for x2 in 0..center_x {
        let x = max_x - x2 - 1;
        if image.get_pixel(x, center_y).to_rgb().data != [255, 255, 255] {
            padding_right =
                if x > image.width() - 10 { image.width() } else { x + 10 };
            break;
        }
    }

    for y in 0..center_y {
        if image.get_pixel(center_x, y).to_rgb().data != [255, 255, 255] {
            padding_top =
                if y < 10 { 0 } else { y - 10};
            break;
        }
    }

    for y2 in 0..center_y {
        let y = max_y - y2 - 1;
        if image.get_pixel(center_x, y).to_rgb().data != [255, 255, 255] {
            padding_bottom =
                if y > image.width() - 10 { image.width() } else { y + 10 };
            break;
        }
    }

    let new_width = padding_right - padding_left;
    let new_height = padding_bottom - padding_top;
    let cropped = image.sub_image(padding_left, padding_top, new_width, new_height);

    cropped.to_image()
}
